msgid ""
msgstr ""
"Project-Id-Version: howdy\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-15 14:53+0300\n"
"PO-Revision-Date: 2023-11-14 11:40\n"
"Last-Translator: \n"
"Language-Team: Russian\n"
"Language: ru_RU\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: pygettext.py 1.5\n"
"Plural-Forms: nplurals=4; plural=((n%10==1 && n%100!=11) ? 0 : ((n%10 >= 2 && n%10 <=4 && (n%100 < 12 || n%100 > 14)) ? 1 : ((n%10 == 0 || (n%10 >= 5 && n%10 <=9)) || (n%100 >= 11 && n%100 <= 14)) ? 2 : 3));\n"
"X-Crowdin-Project: howdy\n"
"X-Crowdin-Project-ID: 435228\n"
"X-Crowdin-Language: ru\n"
"X-Crowdin-File: core.pot\n"
"X-Crowdin-File-ID: 2\n"

#: howdy/src/cli.py:21
msgid "Could not determine user, please use the --user flag"
msgstr "Не удалось определить пользователя. Пожалуйста, используйте флаг --user"

#: howdy/src/cli.py:26
msgid "Command line interface for Howdy face authentication."
msgstr "Интерфейс командной строки для модуля аутентификации по лицу Howdy."

#: howdy/src/cli.py:30
msgid "command"
msgstr "команда"

#: howdy/src/cli.py:30
msgid "arguments"
msgstr "аргументы"

#: howdy/src/cli.py:31
msgid ""
"For support please visit\n"
"https://github.com/boltgolt/howdy"
msgstr ""
"За поддержкой пожалуйста обратитесь к\n"
"https://github.com/boltgolt/howdy"

#: howdy/src/cli.py:36
msgid "The command option to execute, can be one of the following: add, clear, config, disable, list, remove, snapshot, set, test or version."
msgstr "Опция для выполнения команды. Может принимать одно из следующих значений: add, clear, config, disable, list, remove, snapshot, set, test или version."

#: howdy/src/cli.py:43
msgid "Optional arguments for the add, disable, remove and set commands."
msgstr "Опциональные аргументы для команд add, disable, remove и set."

#: howdy/src/cli.py:50
msgid "Set the user account to use."
msgstr "Укажите пользователя для использования."

#: howdy/src/cli.py:55
msgid "Skip all questions."
msgstr "Пропустить все вопросы."

#: howdy/src/cli.py:61
msgid "Print machine-friendly output."
msgstr "Показать машиночитаемый вывод."

#: howdy/src/cli.py:69
msgid "Show this help message and exit."
msgstr "Показать данную справку и выйти."

#: howdy/src/cli.py:73
msgid "current active user: "
msgstr "текущий активный пользователь: "

#: howdy/src/cli.py:87
msgid "Please run this command as root:\n"
msgstr "Пожалуйста, выполните эту команду от пользователя root:\n"

#: howdy/src/cli.py:93
msgid "Can't run howdy commands as root, please run this command with the --user flag"
msgstr "Невозможно выполнить команду от имени пользователя root. Пожалуйста, запустите её с флагом --user"

#: howdy/src/cli/add.py:23
msgid ""
"\n"
"Can't import the dlib module, check the output of"
msgstr ""
"\n"
"Не удается импортировать модуль dlib. Проверьте вывод команды"

#: howdy/src/cli/add.py:32 howdy/src/compare.py:49
msgid "Data files have not been downloaded, please run the following commands:"
msgstr "Файлы с данными для модели не были загружены. Пожалуйста, выполните следующие команды:"

#: howdy/src/cli/add.py:58
msgid "No face model folder found, creating one"
msgstr "Не найдена папка для хранения образцов лица. Создаю её"

#: howdy/src/cli/add.py:69
msgid "NOTICE: Each additional model slows down the face recognition engine slightly"
msgstr "ВНИМАНИЕ: Каждый дополнительный образец лица немного замедляет процедуру распознавания"

#: howdy/src/cli/add.py:70
msgid "Press Ctrl+C to cancel\n"
msgstr "Чтобы скопировать нажмите Ctrl+C\n"

#: howdy/src/cli/add.py:74
msgid "Adding face model for the user "
msgstr "Добавление образца лица пользователя "

#: howdy/src/cli/add.py:88
msgid "Model #"
msgstr "Модель №"

#: howdy/src/cli/add.py:92
#, python-format
msgid "Using default label \"%s\" because of -y flag"
msgstr "Использование метки по умолчанию \"%s\", так как установлен флаг -y"

#: howdy/src/cli/add.py:95
msgid "Enter a label for this new model [{}]: "
msgstr "Введите название для нового образца лица [{}]: "

#: howdy/src/cli/add.py:103
msgid "NOTICE: Removing illegal character \",\" from model name"
msgstr "ВНИМАНИЕ: Удаление недопустимого символа \",\" из названия образца лица"

#: howdy/src/cli/add.py:117
msgid ""
"\n"
"Please look straight into the camera"
msgstr ""
"\n"
"Пожалуйста, смотрите прямо в камеру"

#: howdy/src/cli/add.py:182
msgid "Camera saw only black frames - is IR emitter working?"
msgstr "Камера видит только черные кадры. ИК-датчик активен и работает корректно?"

#: howdy/src/cli/add.py:184 howdy/src/compare.py:237
msgid "All frames were too dark, please check dark_threshold in config"
msgstr "Все кадры получились слишком темными. Пожалуйста, проверьте опцию dark_threshold в конфигурации"

#: howdy/src/cli/add.py:185 howdy/src/compare.py:238
#, python-brace-format
msgid "Average darkness: {avg}, Threshold: {threshold}"
msgstr "Средняя \"темнота\": {avg}, Пороговое значение: {threshold}"

#: howdy/src/cli/add.py:187
msgid "No face detected, aborting"
msgstr "Лицо не обнаружено, завершение"

#: howdy/src/cli/add.py:192
msgid "Multiple faces detected, aborting"
msgstr "Обнаружено несколько лиц в кадре"

#: howdy/src/cli/add.py:213
msgid ""
"\n"
"Scan complete\n"
"Added a new model to "
msgstr ""
"\n"
"Сканирование завершено\n"
"Добавлен новый образец лица пользователя "

#: howdy/src/cli/clear.py:16
msgid "No models created yet, can't clear them if they don't exist"
msgstr "Отсутствуют образцы лица пользователя, поэтому невозможно их удалить"

#: howdy/src/cli/clear.py:21
msgid "{} has no models or they have been cleared already"
msgstr "не имеет сохраненных образцов лица или они уже были удалены"

#: howdy/src/cli/clear.py:27
msgid "This will clear all models for "
msgstr "Будут удалены все модели для пользователя "

#: howdy/src/cli/clear.py:28 howdy/src/cli/remove.py:53
msgid "Do you want to continue [y/N]: "
msgstr "Вы все равно хотите продолжить? [y/N]: "

#: howdy/src/cli/clear.py:32 howdy/src/cli/remove.py:57
msgid ""
"\n"
"Interpreting as a \"NO\", aborting"
msgstr ""
"\n"
"Интерпретировано как \"НЕТ\", остановка"

#: howdy/src/cli/clear.py:37
msgid ""
"\n"
"Models cleared"
msgstr ""
"\n"
"Образцы лица удалены"

#: howdy/src/cli/config.py:11
msgid "Opening config.ini in the default editor"
msgstr "Открытие config.ini в редакторе по умолчанию"

#: howdy/src/cli/disable.py:22
msgid "Please add a 0 (enable) or a 1 (disable) as an argument"
msgstr "Пожалуйста, добавьте 0 (включить) или 1 (отключить) в качестве аргумента команды"

#: howdy/src/cli/disable.py:35
msgid "Please only use 0 (enable) or 1 (disable) as an argument"
msgstr "Пожалуйста, используйте только 0 (включить) или 1 (отключить) в качестве аргумента команды"

#: howdy/src/cli/disable.py:40
msgid "The disable option has already been set to "
msgstr "Для опции отключения уже было установлено значение "

#: howdy/src/cli/disable.py:49
msgid "Howdy has been disabled"
msgstr "Модуль Howdy отключен"

#: howdy/src/cli/disable.py:51
msgid "Howdy has been enabled"
msgstr "Модуль Howdy активирован"

#: howdy/src/cli/list.py:17 howdy/src/cli/remove.py:25
msgid "Face models have not been initialized yet, please run:"
msgstr "Хранилище образцов лиц еще не было инициализировано. Пожалуйста, выполните команду:"

#: howdy/src/cli/list.py:29 howdy/src/cli/remove.py:36
msgid "No face model known for the user {}, please run:"
msgstr "Ни одного образца лица еще не было добавлено для пользователя {}. Пожалуйста, выполните команду:"

#: howdy/src/cli/list.py:35
msgid "Known face models for {}:"
msgstr "Найденные образцы лица для пользователя {}:"

#: howdy/src/cli/list.py:36
#, fuzzy
msgid "ID  Date                 Label[0m"
msgstr "ID  Дата                 Название\\033[0m"

#: howdy/src/cli/remove.py:16
msgid "Please add the ID of the model you want to remove as an argument"
msgstr "Пожалуйста, укажите в аргументе команды ID образца лица, который вы хотите удалить"

#: howdy/src/cli/remove.py:17 howdy/src/cli/set.py:18
msgid "For example:"
msgstr "Например:"

#: howdy/src/cli/remove.py:19
msgid "You can find the IDs by running:"
msgstr "ID можно найти, выполнив следующие:"

#: howdy/src/cli/remove.py:52
#, python-brace-format
msgid "This will remove the model called \"{label}\" for {user}"
msgstr "Это удалит образец лица с названием \"{label}\" для пользователя {user}"

#: howdy/src/cli/remove.py:69
#, python-brace-format
msgid "No model with ID {id} exists for {user}"
msgstr "Для пользователя {user} не существует модели с ID {id}"

#: howdy/src/cli/remove.py:75
msgid "Removed last model, howdy disabled for user"
msgstr "Последний образец лица удален, модуль howdy отключен для пользователя"

#: howdy/src/cli/remove.py:89
msgid "Removed model {}"
msgstr "Удален образец лица {}"

#: howdy/src/cli/set.py:17
msgid "Please add a setting you would like to change and the value to set it to"
msgstr "Пожалуйста, добавьте параметр, который вы хотите изменить, и желаемое для него значение"

#: howdy/src/cli/set.py:37
msgid "Could not find a \"{}\" config option to set"
msgstr "Не удалось найти параметр \"{}\""

#: howdy/src/cli/set.py:44
msgid "Config option updated"
msgstr "Файл конфигурации обновлен"

#: howdy/src/cli/snap.py:43
msgid "GENERATED SNAPSHOT"
msgstr "СГЕНЕРИРОВАННЫЙ СЛЕПОК"

#: howdy/src/cli/snap.py:44 howdy/src/compare.py:74
msgid "Date: "
msgstr "Дата: "

#: howdy/src/cli/snap.py:45
msgid "Dark threshold config: "
msgstr "Пороговое значение \"темноты\" в настройках: "

#: howdy/src/cli/snap.py:46
msgid "Certainty config: "
msgstr "Значение уровня достоверности в настройках: "

#: howdy/src/cli/snap.py:50
msgid "Generated snapshot saved as"
msgstr "Созданный snapshot сохранен как"

#: howdy/src/cli/test.py:23
msgid "Howdy has been configured to use a recorder which doesn't support the test command yet, aborting"
msgstr "Howdy настроен на использование модуля захвата изображения, который еще не поддерживает команду test, прерывание"

#: howdy/src/cli/test.py:34
msgid ""
"\n"
"Opening a window with a test feed\n"
"\n"
"Press ctrl+C in this terminal to quit\n"
"Click on the image to enable or disable slow mode\n"
msgstr ""
"\n"
"Открытие окна для тестирования\n"
"\n"
"Нажмите Ctrl+C в терминале для выхода\n"
"Нажмите на изображение, чтобы включить или отключить медленный режим\n"

#: howdy/src/cli/test.py:148
#, python-format
msgid "RESOLUTION: %dx%d"
msgstr "РАЗРЕШЕНИЕ: %dх%d"

#: howdy/src/cli/test.py:149
#, python-format
msgid "FPS: %d"
msgstr "FPS: %d"

#: howdy/src/cli/test.py:150
#, python-format
msgid "FRAMES: %d"
msgstr "КАДРЫ: %d"

#: howdy/src/cli/test.py:151
#, python-format
msgid "RECOGNITION: %dms"
msgstr "РАСПОЗНОВАНИЕ: %dms"

#: howdy/src/cli/test.py:155
msgid "SLOW MODE"
msgstr "МЕДЛЕННЫЙ РЕЖИМ"

#: howdy/src/cli/test.py:160
msgid "DARK FRAME"
msgstr "ТЁМНЫЙ КАДР"

#: howdy/src/cli/test.py:163
msgid "SCAN FRAME"
msgstr "ОТСКАНИРОВАННЫЙ КАДР"

#: howdy/src/cli/test.py:247
msgid ""
"\n"
"Closing window"
msgstr ""
"\n"
"Закрытие окна"

#: howdy/src/compare.py:73
msgid " LOGIN"
msgstr " ЛОГИН"

#: howdy/src/compare.py:75
msgid "Scan time: "
msgstr "Время сканирования: "

#: howdy/src/compare.py:76
msgid "Frames: "
msgstr "Кадры: "

#: howdy/src/compare.py:77
msgid "Hostname: "
msgstr "Имя компьютера: "

#: howdy/src/compare.py:78
msgid "Best certainty value: "
msgstr "Наилучший уровень достоверности: "

#: howdy/src/compare.py:163
#, fuzzy
msgid "Starting up..."
msgstr "Запуск"

#: howdy/src/compare.py:211
msgid "Identifying you..."
msgstr "Идентифицирую вас..."

#: howdy/src/compare.py:234
msgid "FAILED"
msgstr "ОШИБКА"

#: howdy/src/compare.py:335
msgid "Time spent"
msgstr "Затрачено времени"

#: howdy/src/compare.py:336
msgid "Starting up"
msgstr "Запуск"

#: howdy/src/compare.py:337
#, python-format
msgid "  Open cam + load libs: %dms"
msgstr "  Открытие камеры + загрузка библиотек: %dms"

#: howdy/src/compare.py:338
msgid "  Opening the camera"
msgstr "  Открытие камеры"

#: howdy/src/compare.py:339
msgid "  Importing recognition libs"
msgstr "  Импорт библиотек для распознования"

#: howdy/src/compare.py:340
msgid "Searching for known face"
msgstr "Поиск образцов лиц"

#: howdy/src/compare.py:341
msgid "Total time"
msgstr "Общее время"

#: howdy/src/compare.py:343
msgid ""
"\n"
"Resolution"
msgstr ""
"\n"
"Разрешение"

#: howdy/src/compare.py:345
#, python-format
msgid "  Native: %dx%d"
msgstr "  Нативное: %dx%d"

#: howdy/src/compare.py:348
#, python-format
msgid "  Used: %dx%d"
msgstr "  Фактически использованное: %dx%d"

#: howdy/src/compare.py:351
#, python-format
msgid ""
"\n"
"Frames searched: %d (%.2f fps)"
msgstr ""
"\n"
"Всего кадров: %d (%.2f fps)"

#: howdy/src/compare.py:352
#, python-format
msgid "Black frames ignored: %d "
msgstr "Проигнорировано черных кадров: %d "

#: howdy/src/compare.py:353
#, python-format
msgid "Dark frames ignored: %d "
msgstr "Проигнорировано темных кадров: %d "

#: howdy/src/compare.py:354
#, python-format
msgid "Certainty of winning frame: %.3f"
msgstr "Уровень достоверности для лучшего кадра: %.3f"

#: howdy/src/compare.py:356
#, python-format
msgid "Winning model: %d (\"%s\")"
msgstr "Лучший образец лица: %d (\"%s\")"

#: howdy/src/compare.py:360
msgid "SUCCESSFUL"
msgstr "УСПЕШНО"

#: howdy/src/pam/main.cc:73
msgid "Failure, timeout reached"
msgstr "Ошибка, лимит времени исчерпан"

#: howdy/src/pam/main.cc:80
msgid "Face detection image too dark"
msgstr "Кадр слишком тёмный"

#: howdy/src/pam/main.cc:89
msgid "Unknown error: "
msgstr "Неизвестная ошибка: "

#: howdy/src/pam/main.cc:123
msgid "Identified face as {}"
msgstr "Определил лицо как {}"

#: howdy/src/pam/main.cc:265
msgid "Attempting facial authentication"
msgstr "Аутентификация по лицу"

#: howdy/src/pam/main.cc:395
msgid "Insufficient permissions to send Enter press, waiting for user to press it instead"
msgstr "Недостаточно привилегий, чтоб отправить нажание Enter, ожидается нажатие пользователем"

#: howdy/src/pam/main.cc:415 howdy/src/pam/main.cc:420
msgid "Failed to send Enter press, waiting for user to press it instead"
msgstr "Не удалось отправить нажатие Enter, ожидается нажатие пользователем"

#: howdy/src/recorders/ffmpeg_reader.py:16
msgid "Missing ffmpeg module, please run:"
msgstr "Отсутствует модуль ffmpeg. Пожалуйста, выполните следующую команду:"

#: howdy/src/recorders/pyv4l2_reader.py:15
msgid "Missing pyv4l2 module, please run:"
msgstr "Отсутствует модуль pyv4l2. Пожалуйста, выполните следующую команду:"

#: howdy/src/recorders/video_capture.py:38
msgid "Howdy could not find a camera device at the path specified in the config file."
msgstr "Howdy не смог найти устройство камеры по пути, указанному в конфигурационном файле."

#: howdy/src/recorders/video_capture.py:39
msgid "It is very likely that the path is not configured correctly, please edit the 'device_path' config value by running:"
msgstr "Вероятно, что путь к устройству камеры настроен неверно. Пожалуйста, отредактируйте значение параметра конфигурации \"device_path\", запустив следующую команду:"

#: howdy/src/recorders/video_capture.py:88
msgid "Failed to read camera specified in the 'device_path' config option, aborting"
msgstr "Не удалось считать данные с устройства камеры, указанной в параметре конфигурации \"device_path\""

#: howdy/src/rubberstamps/__init__.py:80
msgid "Error parsing rubberstamp rule: {}"
msgstr ""

#: howdy/src/rubberstamps/__init__.py:87
msgid "Stamp not installed: {}"
msgstr ""

#: howdy/src/rubberstamps/__init__.py:97
msgid "Stamp error: Class {} not found"
msgstr ""

#: howdy/src/rubberstamps/__init__.py:123
msgid "Internal error in rubberstamp configuration declaration:"
msgstr ""

#: howdy/src/rubberstamps/__init__.py:162
msgid "Internal error in rubberstamp:"
msgstr ""

#: howdy/src/rubberstamps/hotkey.py:21
msgid "Aborting authorisation in {}"
msgstr ""

#: howdy/src/rubberstamps/hotkey.py:21
msgid "Authorising in {}"
msgstr ""

#: howdy/src/rubberstamps/hotkey.py:25
#, python-brace-format
msgid "Press {abort_key} to abort, {confirm_key} to authorise"
msgstr ""

#: howdy/src/rubberstamps/hotkey.py:50
msgid "Authentication aborted"
msgstr ""

#: howdy/src/rubberstamps/nod.py:17
msgid "Nod to confirm"
msgstr ""

#: howdy/src/rubberstamps/nod.py:18
msgid "Shake your head to abort"
msgstr ""

#: howdy/src/rubberstamps/nod.py:81
msgid "Confirmed authentication"
msgstr ""

#: howdy/src/rubberstamps/nod.py:84
msgid "Aborted authentication"
msgstr ""

#~ msgid ""
#~ "\n"
#~ "Inerpeting as a \"NO\", aborting"
#~ msgstr ""
#~ "\n"
#~ "Интерпретировано как \"НЕТ\", остановка"
